# programa que lea numeros hasta que el usuario escriba "fin"
# se debe imprimir el total
# cantidad de numeros
# la media de esos numeros
# si se introduce algo q no sea un numero detecte un fallo

# autor = "Michael Vasquez"
# email = "michael.vasquez@unl.edu.ec"

cont = 0
total = 0

while True:
    valor = input("Introduce un número entero (o 'fin' para terminar): ")
    if valor.lower() in "fin":
        break
    try:
        total += int (valor)
        cont  += 1
        media = total/cont
    except ValueError:
        print("Valor introducido incorrecto. Intenta de nuevo...")

print("El total es: ", total)
print("Haz introducido: ", cont, " numeros")
print("La media de los valores es: ", float(media), "=", int(media))